# Browsing on your Mobile (Hands-on)
--- meta
title: Browsing on your Mobile (Hands-on)
uuid: 8c82c04d-2566-4081-954b-030db1031ba1
locale: en
source: Tactical Tech
item: SLE
tags:
  - Mobile
  - Browser
  - Hands-on
duration: 200
description: See what gets shared with others through your browser, and how. Then take concrete steps to make your web browsing more private and secure.
---

## Meta information

### Workshop Description
See what gets shared with others through your browser, and how. Then take concrete steps to make your web browsing more private and secure.

### Overview
1. Introductions (10 min)
2. The Browser is a two-way street (60 min)
3. Tracking in the browser
4. Browsing on your mobile: Hands-on  (60 min)
5. Search engines (20 min)
6. Strategise (30 min)
7. Wrap up (20 min)


### Duration
200 minutes (excluding breaks)

### Ideal Number of Participants
This session splits up participants into two groups according to their operating systems, iOS or Android, so there should be a mimimum of two facilitators, ideally with a third facilitator to step in where needed.

- 2-20 participants: 2-3 trainers
- 20-28 participants: 4 trainers

### Learning Objectives
##### Knowledge
- See how and why your browser might not be as secure as you might think, and where insecurities lie.
- Understand what 'encryption' means, how the internet works (infrastructure), and how and why online tracking fuels advertising.
- Know what is meant by 'anonymity' online, and understand the basics of Tor.

##### Skills
- Be able to make informed decisions about which browsers to use
- Be able to limit data traces left through the browser
- *Android*: Learn how to install and customise Firefox, and install and use Orbot and Orfox
- *iPhone*: Learn how to customise Safari to limit tracking.

##### Attitude
- Choices can be made and simple actions taken to limit tracking.

### References
- Download the Mobiles Reference Document from the link above, or find it on the Materials page of MyShadow.org: https://myshadow.org/materials
- EFF's diagram on HTTPS and Tor: [Tor and HTTPS](https://www.eff.org/pages/tor-and-https)
- [Trackography](https://trackography.org) (Tactical Tech)
- [Lightbeam](https://myshadow.org/resources/lightbeam?locale=en)
- [Panopticlick](https://myshadow.org/resources/panopticlick?locale=en) (EFF)
- [Alternatives](https://myshadow.org/increase-your-privacy#alternatives) (MyShadow.org / Tactical Tech)

### Materials and Equipment Needed
- @[material](6d758ada-e6cf-4a56-a96b-f84dfe14181c)
- @[material](6ebfc3d0-3f8a-41b9-800b-5e802b985fd8)
- @[material](412c1eb9-9fc6-4f6b-975a-6d7c1915f750)
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](9392dacf-999c-4c33-a6d8-4545c1aee849)
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](884b8427-f8ba-441c-a663-b32282aaeb41)
- @[material](9824a3dd-12ed-4684-9afa-dd75118404ee)

### Optional Materials and Handouts
- @[material](9824a3dd-12ed-4684-9afa-dd75118404ee)

## Steps

### Step 1: Introductions (10 min)
1. Briefly introduce yourself and the session, then ask participants to introduce themselves and to answer the following questions:
    - What browsers do you use on your mobile, and why do you use these specific browsers?
    - What do you want to learn in this session?
2. Taking expectations into account, give a brief overview of the session, including objectives, what will be covered (and what not), and how much time is available.

### Step 2: The browser as two-way street (30 min)
While the browser allows us to access the internet, it also allows others to access lots of information about _us_.

The following activity gives an overview of how the internet works, showing shows what information third parties can see when you browse the web or send email. It then shows what others can see when you use the internet via https and via Tor.


#### Step 3: Tracking in the browser (30 min)
Tracking in the browser is often invisible. The following activity gives participants the chance to see how tracking works.


### Step 4: Browsing on your mobile: Hands- On Session (60 min)
Run a hands-on session that covers the following: (find detailed information in the _Mobiles Reference Document_)

1. **Compare, choose, and customise**
    - Go through an overview of the different browsers available for the OS you're focusing on, and then:
      - **Android:** Install and customise Firefox (only browser on Android where settings can be adjusted).
      - **iPhone:** Customise Safari (only browser on iPhone where settings can be adjusted).

2. **Install a VPN or Tor**
    - **Android**
      - Install Orfox and Orbot
      - Discuss alternative app stores and help participants configure their phone settings to accept apps from other sources if they want to use these alternatives.

    - **iPhone**:
      - Since Tor (via Orfox and Orbot) can not be used via an iPhone, a VPN is the next best option. Show participants how to install a VPN, and have them set one up if possible.


### Step 5: Search engines (20 min)
Use the following activity to help participants understand important differences between commercial and 'alternative' tools, with a focus on Search Engines.

### Step 6: Strategies of Resistance (30 min)
Use the following activity to guide participants through key strategies for taking more control of the data they share with commercial companies. Adapt it so that it specifically focuses on Browsing.

### Step 7: Wrap up (15 min)
1. See if anything is unclear, and answer questions
2. Direct participants to resources
3. Hand out Tactical Tech's Pocket Privacy guide for Mobiles if you have them.


-------------------------------
<!---
data-privacy-training/Exhibition/TEMPLATE
-->
