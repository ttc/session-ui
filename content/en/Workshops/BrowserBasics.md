# Browser Basics
--- meta
title: Browser Basics
uuid: 9d3644ef-090b-496d-9d9a-f242e84ff196
locale: en
source: Tactical Tech
item: SLE
tags:
  - Browser
duration: 120
description: How are different browsers different, why does this matter, and what types of information about yourself are you giving away?
---

## Meta information

### Description
How are different browsers different, why does this matter, and what types of information about yourself are you giving away?

### Overview
1. Introductions (10 min)
2. You and Your browser (30 min)
3. Follow-up Discussion (10 min)
4. Unique browser fingerprint (10 min)
5. Comparing different browsers (20 min)
6. Tracking in the browser (30 min)
7. Wrap up (10 min)


### Duration
120 minutes (excluding breaks)

### Number of Participants
10 - 20.  


### Learning Objectives
##### Knowledge
- Understand how browsers are different and why this matters.
- Understand what data is collected through the browser.
- Know what a browser fingerprint is, and what is meant by the term 'third party tracker'.

### References
- [Panopticlick](https://myshadow.org/panopticlick) (EFF)
- [Trackography](https://trackography.org) (Tactical Tech)
- [Lightbeam](https://www.mozilla.org/en-US/lightbeam/) (installation in Firefox browser required)


### Materials and Equipment Needed
- @[material](0d1c2469-bc55-41da-8207-63edf8fd307b)
- @[material](ce457811-1423-4ff0-93bb-7bc2fda1e844)
- @[material](67c7149b-44f9-4b7a-b4ee-c8bf786b50dc)
- @[material](1544909d-e400-4588-a4cb-b638a204422e)
- @[material](b6be8eed-7382-4594-bbe1-eaf471f8f081)
- @[material](e664e54e-f6cb-4e88-bc1e-cbdfed060456)

### Optional Materials and Handouts
- @[material](ce39f7f2-0357-419a-aca7-81806e00e6cf)

## Steps

### Step 1: Introductions (10 min)
1. Briefly introduce yourself and the session, then ask participants to introduce themselves and answer the following questions:  
    - What browsers do you use, and what do you use these for?
    - What do you want to learn in this session?
2. Taking expectations into account, give a brief overview of the session, including objectives, what will be covered (and what not), and how much time is available.


### Step 2: You and Your browser (30 min)
The following activity is designed to make everyday invisible actions in the browser visible and personal. Ask participants to draw what they did in their browser the previous day or week.


##### Activity: "Draw Your Browser History"
@[activity](ad052ce9-29c9-4f76-b94e-0c203a2079f0)

### Step 3: Follow-up Discussion (10 min)
Use the drawing exercise as a starting point for a discussion which draws out the following points:
1. From what you do in the browser (visit website, do searches, etc), what data can be _collected_ from the browser? ie What can others know about your activity? (Browser history, ie websites visited, searches, device information, IP address, time stamp etc).
2. How is this data collected? (Browser fingerprinting and third party trackers: cookies, web beacons etc)

### Step 4: Unique browser fingerprint (10 min)
Open Firefox, go to [Panopticlick](https://panopticlick.eff.org/) to show how unique the browser is. This uniqueness is known as the browser fingerprint. Walk participants through the test and click 'full results' to show all the details.

### Step 5: Comparing different browsers (20 min)
Refer back to the question at the beginning of the session: What browsers do you use, and why? Walk participants through the benefits and limitations of the main browsers available. (Use the _Browser Reference Document_)

### Step 6: Tracking in the browser (30 min)
The following activity allows participants to see in a very visual way how tracking in the browser actually works.


##### Activity: "Visualising tracking in the browser"

### Step 7: Wrap up (10 min)
1. See if anything is still unclear, and answer questions
2. Direct participants to resources
3. Hand out Tactical Tech's _Pocket Privacy guide - Browser_ if you have them.


-------------------------------
<!---
data-privacy-training/SLEs/TEMPLATE
-->
